package com.bank;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Scanner;

public class Login {

	public static void login(AccountDetails ac, Session session){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter below details to Login");
		System.out.println("Account Number : ");
		String accNo = sc.next();
		System.out.println("Account Pin : ");
		int pinNo = sc.nextInt();

		try {
			// Create a query to retrieve the user with the given username and password
			System.out.println(accNo + " => " + pinNo);
			String hql = "FROM Authenticate WHERE accountNo = :accountNo AND pin = :pin";
			Query<Authenticate> query = session.createQuery(hql);
			query.setParameter("accountNo", accNo);
			query.setParameter("pin", pinNo);
			List<Authenticate> authuser = query.list();
			if(authuser.size() == 0)
			{
				System.out.println("No account found with this credentials. Enter valid A/C No. and Pin");
				login(ac, session);
			}
			else if(authuser.size() == 1)
			{
				System.out.println("Login Successfully");
				dashboard(ac, accNo, pinNo);
			}
			else
			{
				System.out.println("System failure! Under maintenance");
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public static void dashboard(AccountDetails ac, String accNo, int pinNo) {
//		boolean flag = true;
//		while(flag){
		System.out.println("Welcome to Bank.com");
		Scanner sc = new Scanner(System.in);
		System.out.println("What would you like to do now?");
		System.out.println("1. Deposit");
		System.out.println("2. Withdraw");
		System.out.println("3. Bank Transfer");
		System.out.println("4. UPI Transfer");
		System.out.println("5. Exit");
		int ch = sc.nextInt();
		switch (ch) {
			case 1:
				System.out.println(" Deposit");
				System.out.println("Enter Deposit Amount:");
				double amount = sc.nextDouble();
				Deposit deposit = new Deposit(ac, amount, accNo);
				Thread thread2 = new Thread(deposit, "Deposit Thread 1");
				thread2.start();
				break;
			case 2:
				System.out.println("Withdraw");
				System.out.println("Enter Withdraw Amount:");
				double withdeawamount = sc.nextDouble();
				Withdraw withdraw = new Withdraw(ac, withdeawamount, accNo);
				Thread thread1 = new Thread(withdraw, "Withdraw Thread 1");
				thread1.start();
				break;
			case 3:
				System.out.println("Bank Transfer");
				System.out.println("Account No to Transfer :");
				String toAcNo = sc.next();
				System.out.println("Amount to Transfer :");
				double amount1 = sc.nextDouble();
				BankTransfer transaction = new BankTransfer(ac, amount1, accNo, toAcNo);
				Thread thread3 = new Thread(transaction, "Transaction Thread 1");
				thread3.start();
				break;
			case 4:
				System.out.println("UPI Transfer");
				break;
			case 5:
				System.out.println("Thanks!");
//				flag = false;
				break;
			default:
				System.out.println("Please make appropriate choose");
		}
	}
//	}
}

package com.bank;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Withdraw implements Runnable {
    private static final Lock lock = new ReentrantLock();
    private final AccountDetails account;
    private double updatedBalance;
    private double withdrawamount = 100;
    //    private Session session;
    private String acNo;

    public Withdraw(AccountDetails account, double withdrawamount, String acNo) {
        this.account = account;
        this.withdrawamount = withdrawamount;
//        this.session = session;
        this.acNo = acNo;
    }

    public double getUpdatedBalance() {
        return updatedBalance;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            withdraw(withdrawamount);
            updatedBalance = account.getBalance();
            return;
        } catch (Exception e) {
            System.out.println("Withdrawal failed:" + e.getMessage());
        } finally {
            lock.unlock();
        }
    }

    public synchronized void withdraw(double amount) throws Exception {
//        perform the withdrawal
        int balance = 0;
        int srno = 0;
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        String hql = "SELECT balance, srNo FROM AccountDetails WHERE accountNo = :accountNo";
        Query<Object[]> query = session.createQuery(hql);
        query.setParameter("accountNo", acNo);
        List<Object[]> balanceList = query.list();
        for (Object[] result : balanceList) {
            balance = (Integer) result[0];
            srno = (Integer) result[1];
        }
        int currBalance = balance;
        if (amount <= 0) {
            throw new IllegalArgumentException("Withdrawal amount should be positive");
        }
        if (currBalance < amount) {
            throw new Exception("Insufficient balance.");
        }
        currBalance -= amount;

        AccountDetails account = session.get(AccountDetails.class, srno);
        account.setBalance(currBalance);
        session.getTransaction().commit();
        System.out.println("Amount " + amount + " is withdrew successfully.");
        System.out.println("Your current balance is : " + currBalance);
        session.flush();
        session.close();
        return ;
    }
}

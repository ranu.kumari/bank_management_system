package com.bank;

import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class BankMainApplication {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you already have an account? [y/n]");
        String choice = sc.next();
        Accounts ac = new Accounts();
        Authenticate acAuth = new Authenticate();
        String accountNo;
        int pin;

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        if (choice.equalsIgnoreCase("y")) {

            Login.login(ac, session);

        } else {
            System.out.println("Welcome to create a new account.");
            System.out.println("Enter your personal detail - ");
            System.out.println("Account Holder Full Name:");
            sc.nextLine();
            String name = sc.nextLine();

            System.out.println("Date of Birth (yyyy-MM-dd): ");
            String dateOfBirth = sc.next();
            try {
                if (!Accounts.isValidDateOfBirth(dateOfBirth))
                    throw new Exception("Please Enter DOb in the yyyy-MM-dd format and not after today");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                System.out.println("Date of Birth (yyyy-MM-dd): ");
                dateOfBirth = sc.next();
            }
            // System.out.println("Holder Age:");
            int age = Accounts.calculateAge(dateOfBirth);
            System.out.println("Age : " + age);
            try {
                if (age < 18) {
                    throw new Exception("You should be atleast 18 to create a bank account");
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                return;
            }
            System.out.println("PAN Card No.:");
            String panNumber = sc.next();
            System.out.println("Aadhar Card No.:");
            String aadharNumber = sc.next();
            try {
                if (!Accounts.isValidAadharNumber(aadharNumber))
                    throw new Exception("Please enter aadhar number in 12 digits");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                System.out.println("Aadhar Card No.:");
                aadharNumber = sc.next();
            }
            System.out.println("Mobile No.:");
            String mobileNumber = sc.next();
            try {
                if (!Accounts.isValidMobileNumber(mobileNumber))
                    throw new Exception("Please enter mobile number in 10 digits");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                System.out.println("Mobile No.:");
                mobileNumber = sc.next();
            }
            System.out.println("Email Address:");
            String emailAddress = sc.next();
            try {
                if (!Accounts.isValidEmailAddress(emailAddress))
                    throw new Exception("Please enter correct Email Address like abc@gmail.com");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                System.out.println("Email Address:");
                emailAddress = sc.next();
            }
            String upi = String.join("", name.toLowerCase().split(" "))+"@bank.com";

            System.out.println("Address:");
            sc.nextLine();
            String homeAddress = sc.nextLine();


            String accountType = null;
            System.out.println("Select account type:");
            System.out.println("1. Current Account");
            System.out.println("2. Saving Account");
            System.out.println("3. Salary Account");
            int ch = sc.nextInt();
            switch (ch) {
                case 1:
                    accountType = "Current Account";
                    accountNo = Accounts.generateAccountNo();
                    pin = Accounts.generatePin();
                    acAuth.setAccountNo(accountNo);
                    acAuth.setPin(pin);
                    break;
                case 2:
                    accountType = "Saving Account";
                    accountNo = Accounts.generateAccountNo();
                    pin = Accounts.generatePin();
                    acAuth.setAccountNo(accountNo);
                    acAuth.setPin(pin);
                    break;
                case 3:
                    accountType = "Salary Account";
                    accountNo = Accounts.generateAccountNo();
                    pin = Accounts.generatePin();
                    acAuth.setAccountNo(accountNo);
                    acAuth.setPin(pin);
                    break;
                default:
                    System.out.println("Select appropriate account type");

            }

            ac.createAccount(name, age, dateOfBirth, panNumber, aadharNumber, mobileNumber, emailAddress, homeAddress,
                    accountType,upi);

            ac.setAuth(acAuth);

            System.out.println("Thanks!");
            System.out.println("Your Account has been created successfully, here are your account details");
            System.out.println("Account No : " + acAuth.getAccountNo());
            System.out.println("pin : " + acAuth.getPin());
            System.out.println("Upi ID : "+upi);
            session.save(ac);
            session.save(acAuth);
            session.getTransaction().commit();

            Login.login(ac, session);
        }

        session.close();
        sc.close();

    }

}

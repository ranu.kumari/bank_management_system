package com.bank;

public interface IAccounts {
    public void current();
    public void savings();
    public void salary();
}

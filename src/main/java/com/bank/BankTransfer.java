package com.bank;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankTransfer implements Runnable{
    private static final Lock lock = new ReentrantLock();
    private final AccountDetails account;
    private double amount;
    private String acNo;
    private String toAcNo;

    public BankTransfer(AccountDetails account, double amount, String acNo, String toAcNo) {
        this.account = account;
        this.amount = amount;
        this.acNo = acNo;
        this.toAcNo = toAcNo;
    }

    @Override
    public void run() {
        try {
            System.out.println("I'm runnable run method");
            lock.lock();
            transfer(amount, acNo, toAcNo );
        } catch (Exception e) {
            System.out.println("Withdrawal failed:" + e.getMessage());
        } finally {
            lock.unlock();
        }
    }

    public synchronized void transfer(double amount, String acNo, String toAcNo) throws Exception {

        Withdraw withdraw = new Withdraw(account, amount, acNo);
        Thread thread1 = new Thread(withdraw, "Withdraw Thread 1");
        thread1.start();
//        Withdraw the amount
//        int balance = 0;
//        int srno = 0;
//        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
//        Session session = sessionFactory.openSession();
//        session.beginTransaction();
//
//        String hql = "SELECT balance, srNo FROM AccountDetails WHERE accountNo = :accountNo";
//        Query<Object[]> query = session.createQuery(hql);
//        query.setParameter("accountNo", acNo);
//        List<Object[]> balanceList = query.list();
//        for (Object[] result : balanceList) {
//            balance = (Integer) result[0];
//            srno = (Integer) result[1];
//        }
//        int currBalance = balance;
//        if (amount <= 0) {
//            throw new IllegalArgumentException("Transfer amount should be positive");
//        }
//        if (currBalance < amount) {
//            throw new Exception("Insufficient balance.");
//        }
//        currBalance -= amount;
//
//        AccountDetails account = session.get(AccountDetails.class, srno);
//        account.setBalance(currBalance);
//        session.getTransaction().commit();
//        session.flush();
//        session.close();
        System.out.println("Transaction is in process...");


//        Deposit the amount
        Deposit deposit = new Deposit(account, amount, toAcNo);
        Thread thread2 = new Thread(deposit, "Deposit Thread 1");
        thread2.start();
//        int recbalance = 0;
//        int recsrno = 0;
//
//        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
//        Session s = sFactory.openSession();
//        s.beginTransaction();
//        String hql1 = "SELECT balance, srNo FROM AccountDetails WHERE accountNo = :accountNo";
//        Query<Object[]> query1 = s.createQuery(hql1);
//        query1.setParameter("accountNo", toAcNo);
//        List<Object[]> balanceList1 = query.list();
//        for (Object[] result : balanceList1) {
//            recbalance = (Integer) result[0];
//            recsrno = (Integer) result[1];
//        }
//
//        recbalance += amount;
//
//        AccountDetails recAccount = s.get(AccountDetails.class, recsrno);
//        recAccount.setBalance(recbalance);
//        s.getTransaction().commit();
//        s.flush();
//        s.close();
//
//        System.out.println("Transaction Done Successfully...");
//        System.out.println("Amount " + amount + " is debited on account " + toAcNo);
    }
}
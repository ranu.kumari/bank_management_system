package com.bank;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Deposit implements Runnable{
    private static final Lock lock = new ReentrantLock();
    private final AccountDetails account;
    private double amount;
    private String acNo;

    public Deposit(AccountDetails account, double amount, String acNo) {
        this.account = account;
        this.amount = amount;
        this.acNo = acNo;
    }

    @Override
    public void run() {
        try {
            lock.lock();
            deposit(amount, acNo);
            return;
        } catch (Exception e) {
            System.out.println("Withdrawal failed:" + e.getMessage());
        } finally {
            lock.unlock();
        }
    }

    public synchronized void deposit(double amount, String acNo) throws Exception {
//        perform the deposit
        int balance = 0;
        int srno = 0;

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        String hql = "SELECT balance, srNo FROM AccountDetails WHERE accountNo = :accountNo";
        Query<Object[]> query = session.createQuery(hql);
        query.setParameter("accountNo", acNo);
        List<Object[]> balanceList = query.list();
        for (Object[] result : balanceList) {
            balance = (Integer) result[0];
            srno = (Integer) result[1];
        }

        if (amount <= 0) {
            throw new IllegalArgumentException("Deposit amount should be positive");
        }

        balance += amount;

        AccountDetails account = session.get(AccountDetails.class, srno);
        account.setBalance(balance);
        session.getTransaction().commit();

        System.out.println("Amount " + amount + " is debited on account " + acNo + "successfully");
        System.out.println("Your current balance is : " + balance);
        session.flush();
        session.close();
        return ;
    }
}

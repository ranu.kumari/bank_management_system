package com.bank;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class AccountDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int srNo;
    private String name;
    private int age;
    private String dateOfBirth;
    private String accountType;
    private String panNumber;
    private String aadharNumber;
    private String mobileNumber;
    private String emailAddress;
    private String homeAddress;
    private int balance = 0;
    private int minBalanceRequired;
    private String upi;


    // Constructor
    public AccountDetails(String name, int age, String dateOfBirth, String accountType, String panNumber, String aadharNumber, String mobileNumber, String emailAddress, String homeAddress, int balance, int minBalanceRequired, String upi) {
        this.name = name;
        this.age = age;
        this.dateOfBirth = dateOfBirth;
        this.accountType = accountType;
        this.panNumber = panNumber;
        this.aadharNumber = aadharNumber;
        this.mobileNumber = mobileNumber;
        this.emailAddress = emailAddress;
        this.homeAddress = homeAddress;
        this.balance = balance;
        this.minBalanceRequired = minBalanceRequired;
        this.upi = upi;
    }

    public AccountDetails() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getMinBalanceRequired() {
        return minBalanceRequired;
    }

    public void setMinBalanceRequired(int minBalanceRequired) {
        this.minBalanceRequired = minBalanceRequired;
    }

    public String getUpi() {
        return upi;
    }

    public void setUpi(String upi) {
        this.upi = upi;
    }

    public int getSrNo() {
        return srNo;
    }

    @Override
    public String toString() {
        return "AccountDetails{" +
                "srNo=" + srNo +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", accountType='" + accountType + '\'' +
                ", panNumber='" + panNumber + '\'' +
                ", aadharNumber='" + aadharNumber + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", balance=" + balance +
                ", minBalanceRequired=" + minBalanceRequired +
                ", upi='" + upi + '\'' +
                '}';
    }
}

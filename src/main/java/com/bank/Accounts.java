package com.bank;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue(value = "Accounts")
public class Accounts extends AccountDetails implements IAccounts {

    @OneToOne
    @JoinColumn(name="accountNo")
    Authenticate auth;



    @Override
    public String toString() {
        return "Accounts [auth=" + auth + "]";
    }

    public Authenticate getAuth() {
        return auth;
    }

    public void setAuth(Authenticate auth) {
        this.auth = auth;
    }

    public static String generateAccountNo() {
        int acNo = (int) (Math.random() * 1000000000);
        String accountNo = Integer.toString(acNo);
        return accountNo;
    }

    public static int generatePin() {
        int acPin = (int) (Math.random() * 10000);
        return acPin;
    }

    public static boolean isValidMobileNumber(String mobileNumber) {
        // Regular expression for 10-digit mobile number
        String regex = "^[0-9]{10}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(mobileNumber);

        return matcher.matches();
    }

    public static boolean isValidAadharNumber(String aadharNumber) {
        String regex = "^[0-9]{12}$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(aadharNumber);

        return matcher.matches();
    }

    // Validate email address
    public static boolean isValidEmailAddress(String emailAddress) {
        // Regular expression for email address
        String regex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(emailAddress);

        return matcher.matches();
    }

    public static boolean isValidDateOfBirth(String dateOfBirth) {
        // Date format for date of birth
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);

        try {
            // Parse the date string
            Date dob = dateFormat.parse(dateOfBirth);

            // Additional checks (optional)
            Date currentDate = new Date();
            if (dob.after(currentDate)) {
                return false; // Date of birth is in the future
            }

            // Other checks like minimum age, etc.

            return true;
        } catch (ParseException e) {
            return false; // Invalid date format
        }
    }

    public static int calculateAge(String dateOfBirth) {

        LocalDate birthDate = LocalDate.parse(dateOfBirth);
        //LocalDate localDate = dateOfBirth;
        LocalDate currentDate = LocalDate.now();
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    public void createAccount(String name, int age, String dateOfBirth, String panNumber,
                              String aadharNumber, String mobileNumber, String emailAddress, String homeAddress, String accountType,String upi) {

        setName(name);
        setAge(age);
        setDateOfBirth(dateOfBirth);
        setPanNumber(panNumber);
        setAadharNumber(aadharNumber);
        setMobileNumber(mobileNumber);
        setUpi(upi);
        setEmailAddress(emailAddress);
        setHomeAddress(homeAddress);
        if (accountType == "Current Account") {
            setAccountType("Current Account");
            current();
        } else if (accountType == "Saving Account") {
            setAccountType("Saving Account");
            savings();
        } else {
            setAccountType("Salary Account");
            salary();
        }
    }

    @Override
    public void current() {
        setMinBalanceRequired(0);
    }

    @Override
    public void savings() {
        setMinBalanceRequired(10000);
    }

    @Override
    public void salary() {
        setMinBalanceRequired(30000);
    }
}

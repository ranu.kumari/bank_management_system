package com.bank;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Authenticate")
public class Authenticate {
    @Id
    private String accountNo;
    private int pin;

    @OneToOne(mappedBy = "auth")
    Accounts acc;

    public Authenticate() {
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    @Override
    public String toString() {
        return "Authenticate{" +
                "accountNo='" + accountNo + '\'' +
                ", pin=" + pin +
                '}';
    }
}
